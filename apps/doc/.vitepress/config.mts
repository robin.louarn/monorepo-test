import { defineConfig } from 'vitepress';

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'My Awesome Project',
  description: 'A VitePress Site',
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Examples', link: '/markdown-examples' }
    ],

    sidebar: [
      {
        text: 'Examples',
        items: [
          { text: 'Markdown Examples', link: '/markdown-examples' },
          { text: 'Runtime API Examples', link: '/api-examples' }
        ]
      },
      {
        text: 'Typescript',
        items: [
          { text: 'Readme', link: '/config-typescript/index.md' },
          { text: 'Changelog', link: '/config-typescript/changelog.md' }
        ]
      },
      {
        text: 'Eslint',
        items: [
          { text: 'Readme', link: '/config-eslint/index.md' },
          { text: 'Changelog', link: '/config-eslint/changelog.md' }
        ]
      },
      {
        text: 'Plugin hello',
        items: [
          { text: 'Readme', link: '/plugin-hello/index.md' },
          { text: 'Changelog', link: '/plugin-hello/changelog.md' }
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  }
});
