import { defineConfig } from 'unlighthouse';

export default defineConfig({
  site: 'http://localhost:4173',
  debug: true,
  scanner: {
    device: 'desktop'
  },
  ci: {
    budget: {
      performance: 0.8,
      accessibility: 0.8,
      'best-practices': 0.8,
      seo: 0.8
    }
  }
});
