# my-company

## local publish

```shell
# start verdaccio
pnpm --filter @my-company/docker dev
# configure npm registry
pnpm set registry http://localhost:4873/ --location project
# login to local registry
pnpm login
# publish all public workspaces
pnpm run publish
# clear verdaccio data
rm -rf packages/config-docker/verdaccio/storage/data
```

## gitlab-runner config

consig file : `.gitlab-runner/config.toml`

```toml
  [runners.feature_flags]
    FF_NETWORK_PER_BUILD = true
```

## Source

- [My Minimal TypeScript Monorepo Setup for ESM Libraries](https://gist.github.com/manzt/222c8e8f4ed35e74514eb756e4ba09bc)

## Renovate

```sh
GITHUB_COM_TOKEN=$GITHUB_COM_TOKEN LOG_LEVEL=debug pnpm renovate \
    --platform=gitlab \
    --token $GITLAB_TOKEN \
    --endpoint $CI_API_V4_URL \
    --host-rules [{"matchHost": "https://gitlab.com/api/v4/projects/56880161/packages/npm/","token": "${RENOVATE_TOKEN}", "hostType": "npm"}] \
    $CI_PROJECT_PATH
```
