# @my-company/plugin-hello

## 6.0.3

### Patch Changes

- c5bf4cc: reset

## 6.0.2

### Patch Changes

- dc5be17: Mise a jour des dépendances

## 6.0.1

### Patch Changes

- dbf58e7: Add license and author metadata

## 6.0.0

### Major Changes

- fbbf0b2: 💥 changement de nico

### Patch Changes

- 30ea7f1: Modification du console.log

## 5.1.7

### Patch Changes

- 15054d6: 🧪 test changelog

## 5.1.6

### Patch Changes

- a58861f: 💚 Monter de version pour un test renovate

## 5.1.5

### Patch Changes

- 9457d82: ⬆️ Monter de version de eslint, tsx, vitest

## 5.1.4

### Patch Changes

- 3a7c562: ✅ Mise en place de test e2e et de snapshot
- f9332ad: ⬆️ Mise a jour de dependances

## 5.1.3

### Patch Changes

- 37595f3: 🔧 mise a jour de la configuration de publication

## 5.1.2

### Patch Changes

- cb46953: 🩹 Mise a jour du titre

## 5.1.1

### Patch Changes

- fba7cbf: 📝 ajout du README.md pour la docuementation

## 5.1.0

### Minor Changes

- 1c2f47d: ✏️ Mise a jour du message du plugin hello

## 5.0.0

### Major Changes

- d411ae2: 🔧 configure gitlab-changesets

### Patch Changes

- 33be482: 🐛 fix changesets config

## 4.0.0

### Major Changes

- f4e80cc: ✨ add vue support

### Patch Changes

- Updated dependencies [f4e80cc]
  - @my-company/config-eslint@3.0.0
  - @my-company/config-typescript@3.0.0

## 3.0.0

### Major Changes

- ✨ Ajout du support du typescript et d'un config eslint

### Patch Changes

- Updated dependencies
  - @my-company/config-eslint@2.0.0
  - @my-company/config-typescript@2.0.0

## 2.0.0

### Major Changes

- 💥Test majeur

## 1.0.2

### Patch Changes

- ✨ Création du plugin hello
