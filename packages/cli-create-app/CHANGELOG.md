# @my-company/cli-create-app

## 1.1.3

### Patch Changes

- c5bf4cc: reset
- Updated dependencies [c5bf4cc]
  - @my-company/config-eslint@4.1.2
  - @my-company/config-typescript@4.1.5
  - @my-company/plugin-hello@6.0.3
  - @my-company/verdaccio@2.0.1

## 1.1.2

### Patch Changes

- Updated dependencies [d404d5c]
  - @my-company/config-eslint@4.1.1
  - @my-company/plugin-hello@6.0.2
  - @my-company/verdaccio@2.0.0

## 1.1.0

### Minor Changes

- Utilisation de version provenant de package.json

## 1.0.9

### Patch Changes

- 798695c: pin deps
- dc5be17: Mise a jour des dépendances

## 1.0.8

### Patch Changes

- dbf58e7: Add license and author metadata

## 1.0.7

### Patch Changes

- 15054d6: 🧪 test changelog

## 1.0.6

### Patch Changes

- a58861f: 💚 Monter de version pour un test renovate

## 1.0.5

### Patch Changes

- df5288d: ⬆️ Monté de version des plugins plugin-hello eslint

## 1.0.4

### Patch Changes

- 9457d82: ⬆️ Monter de version de eslint, tsx, vitest
- f5a5423: ⬆️ Monter de version de vite

## 1.0.3

### Patch Changes

- 3a7c562: ✅ Mise en place de test e2e et de snapshot
- f9332ad: ⬆️ Mise a jour de dependances

## 1.0.0

### Major Changes

- 2fd4cbd: ✨ création de la cli create app
