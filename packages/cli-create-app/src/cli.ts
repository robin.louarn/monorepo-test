#!/usr/bin/env node

import nodePlop from 'node-plop';
import { config, DataProps } from './config';

const main = async () => {
  const plop = await nodePlop();
  config(plop);
  const basicProject = plop.getGenerator('basic-project');
  const answers: DataProps = await basicProject.runPrompts();
  await basicProject.runActions(answers);
};

main();
