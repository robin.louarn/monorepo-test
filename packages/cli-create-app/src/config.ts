import { version as configEslintVersion } from '@my-company/config-eslint/package.json';
import { version as configTypescriptVersion } from '@my-company/config-typescript/package.json';
import { version as pluginHelloVersion } from '@my-company/plugin-hello/package.json';
import { existsSync } from 'fs';
import { NodePlopAPI } from 'node-plop';
import { join } from 'path';

const templatePath = new URL('templates', import.meta.url).pathname;

export type DataProps = {
  workDir: string;
  project: string;
};

export const config = (plop: NodePlopAPI): void => {
  plop.setGenerator('basic-project', {
    description: 'Generator basic project',
    prompts: [
      {
        type: 'input',
        name: 'workDir',
        message: 'where is your working directory ?',
        default: existsSync('.generate') ? '.generate' : '.'
      },
      {
        type: 'input',
        name: 'project',
        message: 'What is the name of the project to create?',
        default: 'new-app'
      }
    ],
    actions: () => [
      {
        type: 'addMany',
        destination: '{{workDir}}/{{kebabCase project}}',
        base: templatePath,
        templateFiles: join(templatePath, '**/*'),
        data: {
          count: '{{ count }}',
          pluginHelloVersion,
          configEslintVersion,
          configTypescriptVersion
        }
      }
    ]
  });
};
