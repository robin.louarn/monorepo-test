import {
  closeServer,
  login,
  main,
  Server,
  verdaccioUrl
} from '@my-company/verdaccio';
import { execa } from 'execa';
import { readFileSync } from 'fs-extra';
import nodePlop from 'node-plop';
import { join } from 'path';
import { afterAll, beforeAll, describe, it } from 'vitest';
import type { DataProps } from './config';
import { config } from './config';

const workDir = new URL('../../../playground', import.meta.url).pathname;

let app: Server | undefined;

beforeAll(async () => {
  app = await main({
    clear: true,
    publish: true,
    open: true
  });
});

afterAll(async () => {
  if (app) await closeServer(app);
});

const testCases = [
  {
    project: 'project-test-with-pnpm',
    packager: 'pnpm',
    options: [
      ['install', '--ignore-workspace', '--registry', verdaccioUrl],
      ['lint'],
      ['build']
    ]
  },
  {
    project: 'project-test-with-npm',
    packager: 'npm',
    options: [
      ['install', '--registry', verdaccioUrl],
      ['run', 'lint'],
      ['run', 'build']
    ]
  }
] as const;

describe.each(testCases)(
  'Test cli generate $project with packager: $packager ',
  async ({ project, packager, options }) => {
    const projectPath = join(workDir, project);
    const plop = await nodePlop();
    config(plop);
    const basicProject = plop.getGenerator('basic-project');

    it.sequential('create app', async ({ expect }) => {
      await login(projectPath);
      await execa('rm', ['-rf', project], { cwd: workDir });

      const actions = await basicProject.runActions({
        project,
        workDir: workDir
      } satisfies DataProps);

      // snapshot test
      const changes = actions.changes
        .flatMap(({ type, path }) => {
          if (type === 'add') return [path];
          if (type === 'addMany') return path.match(/(?<=-> ).+/g);
        })
        .filter(Boolean) as Array<string>;

      changes.forEach((change) =>
        expect(readFileSync(change, 'utf8')).toMatchSnapshot()
      );

      // e2e test
      for await (const option of options)
        await execa(packager, option, { cwd: projectPath });
    });
  }
);
