import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    testTimeout: 1000000,
    reporters: ['default', 'junit'],
    outputFile: {
      junit: 'coverage/junit.xml'
    },
    coverage: {
      reporter: ['text', 'json', 'html']
    }
  }
});
