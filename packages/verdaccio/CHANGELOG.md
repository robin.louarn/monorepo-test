# @my-company/verdaccio

## 2.0.1

### Patch Changes

- c5bf4cc: reset

## 2.0.0

### Major Changes

- 296c1a6: Mise en place place de changeset pour les workspaces privés

### Minor Changes

- 8cbe939: Bump des dépendances

## 1.0.1

### Patch Changes

- 798695c: pin deps
- dc5be17: Mise a jour des dépendances
