import { ConfigYaml } from '@verdaccio/types';
import { $ } from 'execa';
import { Server } from 'http';
import path from 'path';
import { ConfigBuilder, runServer } from 'verdaccio';

const logger = console;
export const port = 4873;
export const storageDir = '.storage-verdaccio';
export const logFile = 'verdaccio.log';
export const verdaccioUrl = `http://0.0.0.0:${port}`;

export const login = async (cwd: string) => {
  logger.log(`👤 add temp user to verdaccio`);
  await $({
    cwd,
    stdout: 'inherit'
  })`npm-auth-to-token -u foo -p s3cret -e test@test.com -r ${verdaccioUrl}`;
};

type CreateConfigProps = {
  log: string;
  storage: string;
  withProxy?: boolean;
};

export const createConfig = ({
  log,
  storage,
  withProxy
}: CreateConfigProps): StartServerProps => {
  const config = ConfigBuilder.build()
    .addAuth({
      'auth-memory': {
        users: {
          foo: {
            name: 'foo',
            password: 's3cret'
          }
        }
      }
    })
    .addUplink('npmjs', { url: 'https://registry.npmjs.org/' })
    .addUplink('gitlab', {
      url: 'https://gitlab.com/api/v4/projects/56880161/packages/npm/'
    })
    .addPackageAccess('@my-company/**', {
      access: '$all',
      publish: '$authenticated',
      ...(withProxy && { proxy: 'gitlab' })
    })
    .addPackageAccess('**', {
      access: '$all',
      publish: '$authenticated',
      proxy: 'npmjs'
    })
    .addStorage(path.resolve(storage))
    .getConfig();

  return {
    ...config,
    self_path: '.',
    logs: {
      type: 'file',
      path: log,
      level: 'http',
      format: 'pretty',
      colors: false
    }
  };
};

export type StartServerProps = ConfigYaml & {
  self_path: string;
  logs: Record<string, string | boolean>;
};

export const startServer = async (
  config: StartServerProps
): Promise<Server> => {
  // @ts-expect-error (verdaccio's interface is wrong)
  const server: Server = await runServer(config);
  return await new Promise((resolve) => {
    server.listen(port, () => {
      logger.log(`🌿 verdaccio running on: ${verdaccioUrl}`);
      resolve(server);
    });
  });
};

export const closeServer = (server: Server): Promise<void> =>
  new Promise((resolve) => {
    server.close(() => resolve());
  });
