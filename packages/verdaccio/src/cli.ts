import { Command } from '@commander-js/extra-typings';
import { main } from './main';

const program = new Command()
  .option('-o, --open', 'keep process open')
  .option('-c, --clear', 'clear cache to avoid EPUBLISHCONFLICT errors')
  .option('-p, --publish', 'should publish packages')
  .parse(process.argv)
  .opts();

export type Options = typeof program;

await main(program);
