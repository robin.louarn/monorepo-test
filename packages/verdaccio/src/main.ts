import { $, ExecaError } from 'execa';
import { pathExists, remove } from 'fs-extra';

import path from 'path';
import { Options } from './cli';
import {
  closeServer,
  createConfig,
  logFile,
  login,
  startServer,
  storageDir,
  verdaccioUrl
} from './verdaccio-server';

const logger = console;

export const main = async (options: Options) => {
  const { stdout: rootModule } = await $`pnpm -w root`;
  const root = path.dirname(rootModule);
  const storage = path.resolve(root, storageDir);
  const npmrc = path.resolve(root, '.npmrc');
  const log = path.resolve(root, logFile);

  logger.log(`🎬 starting verdaccio (without proxy)`);
  let server = await startServer(
    createConfig({
      log,
      storage
    })
  );

  try {
    await login(root);

    if (options.clear) {
      // when running e2e locally, clear cache to avoid EPUBLISHCONFLICT errors
      if (await pathExists(storage)) {
        logger.log(`🗑 cleaning up cache (${storage})`);
        await remove(storage);
      }
    }

    if (options.publish) {
      logger.log('📦 building');
      await $({
        cwd: root,
        stdout: 'inherit'
      })`turbo build --filter="./packages/*"`;

      logger.log('🛫 publishing');
      await $({
        cwd: root,
        stdout: 'inherit',
        env: {
          npm_config_registry: verdaccioUrl
        }
      })`changeset publish --no-git-tag`;
    }

    logger.log('🛌 closing verdaccio');
    await closeServer(server);

    logger.log(`🗑 cleaning up .npmrc`);
    await remove(npmrc);

    if (options.open) {
      logger.log('🎬 starting verdaccio (witht proxy)');
      server = await startServer(
        createConfig({
          log,
          storage,
          withProxy: true
        })
      );

      process.on('SIGTERM', async () => {
        logger.log('🛌 closing verdaccio');
        await closeServer(server);
      });

      return server;
    }
  } catch (error) {
    if (error instanceof ExecaError) {
      logger.error(error.message);
      logger.error('FAILLED');
    }
    await closeServer(server);
  }
};
