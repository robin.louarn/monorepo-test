import { defineConfig, Options } from 'tsup';

export default defineConfig((options: Options) => ({
  entry: ['src/cli.ts'],
  format: ['esm'],
  dts: false,
  clean: true,
  minify: false,
  ...options
}));
