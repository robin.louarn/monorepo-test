# @my-company/config-typescript

## 4.1.5

### Patch Changes

- c5bf4cc: reset

## 4.1.4

### Patch Changes

- dbf58e7: Add license and author metadata

## 4.1.3

### Patch Changes

- 15054d6: 🧪 test changelog

## 4.1.2

### Patch Changes

- a58861f: 💚 Monter de version pour un test renovate

## 4.1.1

### Patch Changes

- 3a7c562: ✅ Mise en place de test e2e et de snapshot
- f9332ad: ⬆️ Mise a jour de dependances

## 4.1.0

### Minor Changes

- 5e833d1: 🔧 Simplification de la configuration ts

## 4.0.1

### Patch Changes

- fba7cbf: 📝 ajout du README.md pour la docuementation

## 4.0.0

### Major Changes

- d411ae2: 🔧 configure gitlab-changesets

### Patch Changes

- 33be482: 🐛 fix changesets config

## 3.0.0

### Major Changes

- f4e80cc: ✨ add vue support

## 2.0.0

### Major Changes

- ✨ Ajout du support du typescript et d'un config eslint
