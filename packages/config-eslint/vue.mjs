import eslint from '@eslint/js';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import pluginVue from 'eslint-plugin-vue';
import tseslint from 'typescript-eslint';
import vueParser from 'vue-eslint-parser';
import prettierConfig from './prettier.config.mjs';

export default tseslint.config(
  eslint.configs.recommended,
  ...tseslint.configs.recommended,
  ...pluginVue.configs['flat/recommended'],
  {
    files: ['**/*.vue'],
    languageOptions: {
      parser: vueParser,
      parserOptions: {
        sourceType: 'module',
        parser: {
          ts: tseslint.parser
        }
      }
    }
  },
  eslintPluginPrettierRecommended,
  {
    rules: {
      'prettier/prettier': ['error', prettierConfig]
    }
  },
  {
    ignores: [
      'node_modules/*',
      'dist/*',
      '.vitepress/dist/*',
      'coverage/*',
      '.unlighthouse/*'
    ]
  }
);
