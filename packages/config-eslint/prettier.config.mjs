/**
 * @type {import('prettier').Config}
 */
export default {
  semi: true,
  trailingComma: 'none',
  singleQuote: true,
  printWidth: 80,
  plugins: ['prettier-plugin-packagejson', 'prettier-plugin-organize-imports']
};
