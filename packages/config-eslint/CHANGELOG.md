# @my-company/config-eslint

## 4.1.2

### Patch Changes

- c5bf4cc: reset

## 4.1.1

### Patch Changes

- d404d5c: test version

## 4.1.0

### Minor Changes

- 8cbe939: Bump des dépendances

## 4.0.6

### Patch Changes

- dc5be17: Mise a jour des dépendances

## 4.0.5

### Patch Changes

- dbf58e7: Add license and author metadata

## 4.0.4

### Patch Changes

- 15054d6: 🧪 test changelog

## 4.0.3

### Patch Changes

- a58861f: 💚 Monter de version pour un test renovate

## 4.0.2

### Patch Changes

- 9457d82: ⬆️ Monter de version de eslint, tsx, vitest

## 4.0.1

### Patch Changes

- 3a7c562: ✅ Mise en place de test e2e et de snapshot
- f9332ad: ⬆️ Mise a jour de dependances

## 4.0.0

### Major Changes

- d411ae2: 🔧 configure gitlab-changesets

### Patch Changes

- 33be482: 🐛 fix changesets config

## 3.0.0

### Major Changes

- f4e80cc: ✨ add vue support

## 2.0.0

### Major Changes

- ✨ Ajout du support du typescript et d'un config eslint
