import { FlatCompat } from '@eslint/eslintrc';
import eslint from '@eslint/js';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import tseslint from 'typescript-eslint';
import prettierConfig from './prettier.config.mjs';

const compat = new FlatCompat();

export default tseslint.config(
  ...compat.plugins('turbo'),
  eslint.configs.recommended,
  ...tseslint.configs.recommended,
  eslintPluginPrettierRecommended,
  {
    rules: {
      'prettier/prettier': ['error', prettierConfig]
    }
  },
  {
    ignores: [
      'node_modules/*',
      'dist/*',
      '.vitepress/dist/*',
      'coverage/*',
      '.unlighthouse/*'
    ]
  }
);
