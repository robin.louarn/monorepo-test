const outdent = require('outdent');

/** @type {import("@changesets/types").CommitFunctions} */
exports.default = {
  getAddMessage: async (changeset, _) => {
    return outdent`
      🔖 ${changeset.summary}
    `;
  },

  getVersionMessage: async ({ releases }, _) => {
    return outdent`
      🔖 Releasing ${releases.length} package(s)
  
      Releases:
      ${releases
        .map(({ name, newVersion }) => `  - ${name}@${newVersion}`)
        .join('\n')}
      [skip ci]
    `;
  }
};
